int numberOfBeams(char** bank, int bankSize) {
    int num_row = strlen(bank[0]);
    int up = 0;
    int down = 0;
    int res = 0;
    for (int i = 0; i < bankSize; ++i) {
        for (int j = 0; j < num_row; ++j) {
            down += bank[i][j] == '1' ? 1:0;
        }
        if(down != 0){
            res += up*down;
            up = down;
            down = 0;
        }
    }

    return res;
}

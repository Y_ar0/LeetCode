void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
  int i = m - 1;
  int j = n - 1;
  int last = m + n -1;
  int tmp;
  
  for (last; last >= 0;--last){
    if (i < 0){
      for (int i = last; i >=0; --i) {
        nums1[i] = nums2[j];
        --j;
      }
      return;
    }
    
    if (j < 0){
      return;
    }
    
    if (nums2[j] >= nums1[i]){
      nums1[last] = nums2[j];
      j--;
    }else{
      tmp = nums1[i];
      nums1[i] = nums1[last];        
      nums1[last] = tmp;
      --i;
    }
  }
}

int longestContinuousSubstring(char* s) {
    char* left = s;
    char* right = s + 1;
    int cur = 1;
    int max = 1;

    while (*right != '\0'){
        if (*right - *left != 1){
            if (cur > max){
                max = cur;
            }
            cur = 0;
        }
        cur++;
        left = right;
        right++;

    }

    return max > cur ? max : cur;
}

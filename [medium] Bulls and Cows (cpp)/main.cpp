class Solution {
public:
    std::string getHint(const std::string& secret, const std::string& guess) {
    std::map<char, int> arr;
    auto g_it = guess.begin();

    size_t cows = 0;
    size_t bulls = 0;
    
    for (auto s_it : secret){
      ++arr[s_it];
      if (s_it == *g_it){
        ++bulls;
      }
      ++g_it;
    }

    for(auto it : guess){
      if (arr[it] > 0){
        ++cows;
        --arr[it];
      }
    }

    std::string ret;
    ret += (std::to_string(bulls)+ "A"+ std::to_string(cows-bulls) + "B");
    return ret;
  }

};

#include <iostream>
#include <map>
#include <string>
#include <cassert>

std::string decodeMessage(const std::string& key, const std::string& message) {
    std::string decoded;
    std::map<char, char> decode;
    auto key_p = key.begin();
    auto key_p_end = key.end();
    auto mes_p = message.begin();
    auto mes_p_end = message.end();

    auto chr = 'a';
    for (; key_p < key_p_end ; ++key_p) {
        if (*key_p != ' ' and decode[*key_p] < 'a'){
            decode[*key_p] = chr;
            chr++;
        }
    }

    for (; mes_p < mes_p_end; ++mes_p) {
        if (*mes_p != ' '){
            decoded.push_back(decode[*mes_p]);
        }else{
            decoded.push_back(' ');
        }
    }

    return decoded;
}

int main() {
    assert(decodeMessage("the quick brown fox jumps over the lazy dog",
                         "vkbs bs t suepuv") == "this is a secret");
    assert(decodeMessage("eljuxhpwnyrdgtqkviszcfmabo",
                         "zwx hnfx lqantp mnoeius ycgk vcnjrdb") =="the five boxing wizards jump quickly");
    return 0;
}

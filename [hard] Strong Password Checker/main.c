#include <stdio.h>
#include <string.h>
#include <assert.h>

size_t strongPasswordChecker(char* password) {
    /*
    size_t len                      - len of the line
    size_t res                      - how much to insert|delete
    short flag_a, flag_A, flag_d    - requirements
    size_t rep                      - number of triples of chars
    size_t sublen                   - len of substr of repeated chars
    size_t pot_1, pot_2, pot_3      - num of substr with sublen % 3 ==0|1|2
    size_t sum                      - total num of action
    */

    size_t len = strlen(password);
    size_t res = 0;
    short flag_a = 1;
    short flag_A = 1;
    short flag_d = 1;
    size_t rep = 0;
    size_t sublen = 1;
    size_t pot_1 = 0;
    size_t pot_2 = 0;
    size_t pot_3 = 0;
    size_t sum = 0;

    ///step1: check len
    if (len < 6){
        res += (6 - len);
        if (len <= 3){
            return res;
        }
    }

    if (len > 20){
        res += (len-20);
    }

    ///step2: check requirements
    for (size_t i = 0; i < len - 1; i++) {
        if (flag_a) {
            if (password[i] >= 'a' && password[i] <= 'z') {
                flag_a = 0;
            }
        }

        if (flag_A) {
            if (password[i] >= 'A' && password[i] <= 'Z') {
                flag_A = 0;
            }
        }

        if (flag_d) {
            if (password[i] >= '0' && password[i] <= '9') {
                flag_d = 0;
            }
        }

        if (password[i] == password[i + 1]) {
            ++sublen;
            if (sublen % 3 == 0){
                rep++;
            }
        }else{
            if (sublen >= 3){
                if (sublen % 3 == 0){
                    ++pot_1;
                }
                if (sublen % 3 == 1){
                    ++pot_2;
                }
                if (sublen % 3 == 2){
                    ++pot_3;
                }
            }
            sublen = 1;
        }
    }

    ///step2.1: if we have substr in the end of line
    if (sublen >= 3){
        if (sublen % 3 == 0){
            ++pot_1;
        }
        if (sublen % 3 == 1){
            ++pot_2;
        }
        if (sublen % 3 == 2){
            ++pot_3;
        }
    }

    ///step2.2 : check the last char
    if (flag_a) {
        if (password[len - 1] >= 'a' && password[len - 1] <= 'z') {
            flag_a = 0;
        }
    }

    if (flag_A) {
        if (password[len - 1] >= 'A' && password[len - 1] <= 'Z') {
            flag_A = 0;
        }
    }

    if (flag_d) {
        if (password[len - 1] >= '0' && password[len - 1] <= '9') {
            flag_d = 0;
        }
    }

    ///step3: calc num of actions
    // flag - num of unmatched requirements
    size_t flag = flag_a+flag_A+flag_d;

    //if len > 20, more steps are needed
    if (len > 20){

        //start with removing chars from substrings with sublen % 3 == 0 (removing 1 chr will remove 1 rep)
        while ((res >= 1) && (pot_1 > 0)){
            --res;
            --rep;
            --pot_1;
            ++sum;
        }

        //removing chars from substrings with sublen % 3 == 1 (removing 2 chr will remove 1 rep)
        while ((res >= 2) && (pot_2 > 0)){
            res -= 2;
            --rep;
            --pot_2;
            sum += 2;
        }

        //removing chars from substrings with sublen % 3 == 2 (removing 3 chr will remove 1 rep)
        while ((res >= 3) && (pot_3 > 0)){
            res -= 3;
            --rep;
            --pot_3;
            sum += 3;
        }

        // if there are still extra chars or repetitions, remove the maximum possible number of triples
        if ((res > 0) && (rep > 0)){
            sum += (res/3 * 3);
            rep -= (res/3);
            res = res%3;
        }

        // add remaining
        sum += (res + rep);

        // if num of flags > number of rep, add their subtract
        sum += flag > rep ? (flag-rep) : 0;
        return sum;
    }

    //if len <=20, max of actions can be taken
    res = res>rep?res:rep;
    res = res>flag?res:flag;
    return res;
}


int main() {
    assert(strongPasswordChecker("FFFFFFFFFFFFFFF11111111111111111111AAA") == 23);
    assert(strongPasswordChecker("bbaaaaaaaaaaaaaaacccccc") == 8);
    assert(strongPasswordChecker("ababababababababaaaaa") == 3);
    assert(strongPasswordChecker("bbaaAaa1aaSaaDaaEccR") == 0);
    assert(strongPasswordChecker("aaaaAAAAAA000000123456") == 5);
    assert(strongPasswordChecker("aaaabbbbccccddeeddeeddeedd") == 8);
//    printf("%d", strongPasswordChecker("aaaabbbbccccddeeddeeddeedd"));

    return 0;
}
int numJewelsInStones(char* jewels, char* stones) {
    int sum = 0;
    for (int i = 0; i < strlen(jewels); i++){
        for (int j = 0; j < strlen(stones); j++){
            sum += jewels[i] == stones[j] ? 1 : 0;
        }
    }
    return sum;

}
